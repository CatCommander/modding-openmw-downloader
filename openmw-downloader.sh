#!/bin/bash
set -x

# Function to make sure the proper binaries are installed
# Required packages are:
# - unrar
# - 7z
# - unzip
# - curl

function bin_check {
  which $1

  if [[ $? != "0" ]]
  then
    echo "Please install $1 from your package manager or add it to your $PATH"
    exit 1
  fi
}

bin_check unrar
bin_check 7z
bin_check unzip
bin_check curl

# URL of mod list to get from modding-openmw.com
URL="https://modding-openmw.com/lists/graphics-overhaul/"

# Directory to download/decompress mods to
DIRECTORY="/home/${USER}/DataDrive/OpenMW_Mods"

# Nexus Mods API Key
if [[ -z "${NEXUS_API_KEY}" ]]
then
  echo "Please set your Nexus API Key as an env var via 'export NEXUS_API_KEY=\"KEY_CONTENTS\""
  exit 1
fi

# Set File Separators
OIFS="$IFS"
IFS=$'\n'

# Set Mod_Num to 0 so we can properly number mods as they are downloaded
MOD_NUM="0"

# Create CSV with the info we need
echo "Mod Number,Mod Name,Mod URL,Downloaded,Downloaded Version,Version ID,Usage Instructions" > morrowind_mods.csv


# For each mod in the ${URL}:
# - Get info from the mod page
# - If the mod is from Nexus, download it (if the file doesn't already exist in ${DIRECTORY}
for MOD in $(curl -s "${URL}" | grep 'fa-download'); do

  # Unset variables that are set later in the script
  # Note: Keeping MOD_NUM because we use that to keep track of the spot we're at in the ${URL} list
  unset MOD_NAME MOD_PATH USAGE_INSTRUCTIONS MOD_URL NEXUS_MODNUM MOD_JAON LATEST ID URL FILETYPE FILE DOWNLOADED

  # MOD_NUM is the mod number from the ${URL} list
  MOD_NUM=$((${MOD_NUM} + 1))

  # MOD_NAME is the name of the mod
  MOD_NAME=$(echo ${MOD} | awk -F '/">' '{print $2}' | sed "s/<\/a>//g;s/&\#x27\;//g;s/      //g;s/ /_/g")

  # MOD_PATH is the path to the mod on ${URL}/mods/${MOD_NAME}/ (Used to get Usage Instructions)
  MOD_PATH=$(echo ${MOD_NAME} | sed 's/ /-/g;s/_/-/g;s/\.//g' | sed 's/---/-/g;s/--/-/g'| tr '[:upper:]' '[:lower:]')

  # Some mods have additional usage instructions that require more than just pointing openmw.cfg to the directory
  # This attempts to get those instructions
  # NOTE: Multi-line instruction notes do not get captured properly - this will be fixed later
  USAGE_INSTRUCTIONS=$(curl -s "https://modding-openmw.com/mods/${MOD_PATH}/" | grep -A 1 'Usage Notes' | sed 's/<\/*[^>]*>//g' | tail -n 1 | sed 's/,/ - /g;s/      //g')

  # MOD_URL parses the ${MOD} line for the name of the mod
  MOD_URL=$(echo ${MOD}  | awk -F '><i' '{print $1}' | sed 's/"//g;s/<a href=//g;s/      //g')

  # If the ${MOD_URL} contains 'www.nexus' then get info via the Nexus API
  if [[ $(echo ${MOD_URL} | grep 'www.nexus' | wc -l) != "0" ]]
  then

    # Get Mod Number from Nexus
    NEXUS_MODNUM=$(echo ${MOD_URL} | awk -F / '{print $6}')

    # Get JSON output for mod data
    MOD_JSON=$(curl -s -X GET -H  "accept: application/json" -H "apikey: ${NEXUS_API_KEY}" "https://api.nexusmods.com/v1/games/morrowind/mods/${NEXUS_MODNUM}/files.json" | jq -r '.files[] | select( any(. == "MAIN"))')

    # Get Latest Version of Mod on Nexus
    LATEST=$(echo ${MOD_JSON} | jq '.version' | tail -n 1 | tr -d '"')

    # Sometimes mods don't have versions
    # If a mod doesn't have a version in the JSON output, get the max ID of the mod
    if [[ ${LATEST} != "" ]]
    then
      # Get Latest ID of Mod for Download if multiple versions
      ID=$(echo ${MOD_JSON} | jq --arg LATEST $LATEST 'select(.version==$LATEST).id | max' | sort -nr | head -n 1)
    else
      ID=$(echo ${MOD_JSON} | jq '.id | max' | sort -nr | head -n 1)
    fi

    # Get URL for package download
    URL=$(curl -s -X GET -H  "accept: application/json" -H "apikey: ${NEXUS_API_KEY}" "https://api.nexusmods.com/v1/games/morrowind/mods/${NEXUS_MODNUM}/files/${ID}/download_link.json" | jq -r '.[] | select(.name=="Los Angeles (Premium)") | .URI')

    # Parse URL to get filetype
    FILETYPE=$(echo "${URL}" | awk -F \? '{print $1}' | awk -F . '{print $NF}')

    # Set filename so we know where we are at with the mod list
    FILE="${MOD_NUM}-${MOD_NAME}.${FILETYPE}"

    # If File Exists, don't download it
    if [ ! -f ${DIRECTORY}/${FILE} ]
    then
      # Download mod!
      curl -o ${DIRECTORY}/${FILE} ${URL} && DOWNLOADED="YES" || DOWNLOADED="NO"
    else
      DOWNLOADED="YES"
    fi

  # If the mod is not from Nexus then we did not download it
  else
    DOWNLOADED="NO"
    LATEST="NA"
    ID="NA"
  fi

  # Echo Mod Info to CSV
  echo "${MOD_NUM},${MOD_NAME},${MOD_URL},${DOWNLOADED},${LATEST},${ID},${USAGE_INSTRUCTIONS}" | tee -a morrowind_mods.csv
done

# For downloaded file, extract the archive to ${DIRECTORY}
for FILE in $(ls ${DIRECTORY}| grep -e '7z' -e 'zip' -e 'rar' | sort -n); do 
  TYPE=$(ls ${FILE} | awk -F . '{print $2}')
  BASENAME=${FILE%.*}
  EXTRACT_PATH="${DIRECTORY}/${BASENAME}"
  if [[ ${TYPE} == "zip" ]]; then
    unzip ${DIRECTORY}/${FILE} -d ${EXTRACT_PATH}
  elif [[ ${TYPE} == "rar" ]]; then
    unrar e ${DIRECTORY}/${FILE} ${EXTRACT_PATH}
  elif [[ ${TYPE} == "7z" ]]; then
    7z x ${DIRECTORY}/${FILE} -o${EXTRACT_PATH}
  else
    echo "Unknown filetype"
  fi
done

# Echo data path for OpenMW CFG
for DIR in $(ls ${DIRECTORY} | sort -n | grep -v -e '\.7z' -e '\.zip' -e '\.rar'); do
  echo "data=\"${DIRECTORY}/${DIR}\""
done
